# Stock Data Fetcher

Stock Data Fetcher 是一个用于获取股票数据并存储到 MySQL 数据库的简单 Go 项目。

## 功能特性

- 获取指定股票（例如梦网科技）今天的实时股票数据。

- 将获取的股票数据存储到 MySQL 数据库中，以便后续分析和使用。

## 数据来源

1.腾讯实时接口API

2.ShowAPI 股票数据接口

## 使用方法

### 1. 准备

确保你已经安装了 Go 编程语言和 MySQL 数据库。

### 2. 设置数据库

source ./data/stock.sql

## 3. 启动

go mod tidy
go mod vendor
go run ./main.go

这个 `README.md` 文件简要介绍了项目的功能特性、使用方法和注意事项。根据实际项目的需要，你可以进一步扩展和详细说明项目的其他方面，例如如何处理错误、如何部署到生产环境等。

API1: http://localhost:8080/stock/SZ002123