package reporter

import (
	"mumu_stock_trad/logger"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

var filterUrl = []string{"register"}

// monitor  http被调
func MonitorHttpRequest(path string, resCode string, duration time.Duration) {
	// 过滤无用请求
	for _, u := range filterUrl {
		if strings.Contains(path, u) {
			return
		}
	}
	logger.ReportMonitor(log.Fields{
		"cost_time": strconv.FormatInt(duration.Milliseconds(), 10),
		"res_code":  resCode,
		"ext":       path,
	}, logger.MonitorHttpRequest)
}

func MonitorHttpCall(path string, resCode string, duration time.Duration) {
	logger.ReportMonitor(log.Fields{
		"cost_time": strconv.FormatInt(duration.Milliseconds(), 10),
		"res_code":  resCode,
		"ext":       path,
	}, logger.MonitorHttpCall)
}
