package routes

import (
	"mumu_stock_trad/proxy"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// InitRoute
func InitRoute(hub *proxy.Hub) *gin.Engine {
	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"}, // 允许所有域名
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Type", "Authorization"},
		AllowCredentials: true,
	}))
	router.SetTrustedProxies([]string{"127.0.0.1"})

	sr := router.Group("/decs-mst/")
	{
		loadMisc(sr)

		loadAPI(sr, hub)
	}
	return router
}
