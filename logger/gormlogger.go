package logger

import (
	"context"
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
	gormlogger "gorm.io/gorm/logger"
	"gorm.io/gorm/utils"
)

type glogger struct {
	SlowThreshold             time.Duration
	SourceField               string
	IgnoreRecordNotFoundError bool
	Debug                     bool
}

func NewGlogger() *glogger {
	return &glogger{
		IgnoreRecordNotFoundError: true,
		Debug:                     true,
		SlowThreshold:             time.Second,
	}
}

func (l *glogger) LogMode(gormlogger.LogLevel) gormlogger.Interface {
	return l
}

func (l *glogger) Info(ctx context.Context, s string, args ...interface{}) {
	log.WithContext(ctx).Infof(s, args...)
}

func (l *glogger) Warn(ctx context.Context, s string, args ...interface{}) {
	log.WithContext(ctx).Warnf(s, args...)
}

func (l *glogger) Error(ctx context.Context, s string, args ...interface{}) {
	log.WithContext(ctx).Errorf(s, args...)
}

func (l *glogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	elapsed := time.Since(begin)
	fields := log.Fields{}
	if l.SourceField != "" {
		fields[l.SourceField] = utils.FileWithLineNum()
	}
	sql, rows := fc()
	if rows == -1 {
		fields["rows"] = "-"
	} else {
		fields["rows"] = rows
	}
	fields["cost"] = elapsed

	switch {
	case err != nil && (!errors.Is(err, gorm.ErrRecordNotFound) || !l.IgnoreRecordNotFoundError):
		fields[log.ErrorKey] = err
		log.WithContext(ctx).WithFields(fields).Errorf("%s", sql)
		return
	case l.SlowThreshold != 0 && elapsed > l.SlowThreshold:
		log.WithContext(ctx).WithFields(fields).Warnf("%s , SLOW SQL >= %s ", sql, l.SlowThreshold.String())
	case l.Debug:
		log.WithContext(ctx).WithFields(fields).Infof("%s", sql)
	}
}
