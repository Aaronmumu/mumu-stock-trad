package util

import (
	"encoding/json"
	"fmt"
	"log"
	"mumu_stock_trad/model"
	"net/http"
)

func GetCategory() (data model.APIResponse, err error) {
	appKey := "c9A84EFCb2C34EEdaAACa3Dd3C1D7728"
	apiURL := fmt.Sprintf("https://route.showapi.com/131-58?appKey=%s", appKey)

	// 发送 HTTPS GET 请求
	resp, err := http.Get(apiURL)
	if err != nil {
		log.Fatalf("Error sending request: %v", err)
	}
	defer resp.Body.Close()

	// 解析 JSON 响应
	var apiResponse model.APIResponse
	err = json.NewDecoder(resp.Body).Decode(&apiResponse)
	if err != nil {
		log.Fatalf("Error decoding JSON: %v", err)
	}

	// 检查 API 返回的结果
	if apiResponse.Showapi_res_code != 0 {
		log.Fatalf("API Error: %s", apiResponse.Showapi_res_error)
	}
	return apiResponse, nil
}
