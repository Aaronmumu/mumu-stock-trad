package util

import (
	"database/sql"
	"fmt"
	"log"
	"mumu_stock_trad/config"

	_ "github.com/go-sql-driver/mysql"
)

// ConnectDB 连接 MySQL 数据库
func ConnectDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.DbUser, config.DbPassword, config.DbHost, config.DbPort, config.DbName))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %v", err)
	}

	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("failed to ping database: %v", err)
	}

	log.Println("Connected to MySQL database")
	return db, nil
}
