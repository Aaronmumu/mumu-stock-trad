package vconfig

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func InitViper() {
	viper.SetDefault("Mode", "release")
	viper.SetDefault("LogLevel", "InfoLevel")
	viper.SetDefault("check_status", "true")

	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("./conf") // path to look for the config file in

	// Find and read the config file
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			log.Error("no such config file")
		} else {
			// Config file was found but another error was produced
			log.Error("read config error")
		}
		log.Panicf("read config file fail:%v", err)
		panic(fmt.Sprintf("read config file fail:%v", err))
	}
}
