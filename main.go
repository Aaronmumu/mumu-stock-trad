package main

import (
	"context"
	"fmt"
	"mumu_stock_trad/logger"
	"mumu_stock_trad/model"
	"mumu_stock_trad/proxy"
	"mumu_stock_trad/routes"
	"mumu_stock_trad/service"
	"mumu_stock_trad/vconfig"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func main() {
	// 初始化 1.配置
	vconfig.InitViper()

	// fmt.Println("数据库配置::", viper.GetString("mysql.addr"))

	// 初始化 2.日志
	logger.InitLog()

	log.Info("初始化日志成功")

	// 初始化 3.定时任务

	go func() {
		// 创建一个新的 cron 实例
		c := cron.New()

		// 添加定时任务
		spec := "*/5 * * * *" // 每天凌晨3点执行
		c.AddFunc(spec, func() {
			fmt.Println("执行定时任务...")
			// 在这里执行你的任务代码
			// controller.UpdateStockQtAll()
		})

		// 启动 cron scheduler (调度器)
		c.Start()
		// 程序结束前停止 cron 服务（确保所有任务执行完毕）
		defer c.Stop()

		// 阻塞主 goroutine，保持程序运行
		select {}
	}()

	// 量化交易
	go func() {
		service.RunNetty()
	}()

	// 初始化 4.数据库DB

	model.InitDB()
	defer model.CloseDB()

	// 初始化 5.路由配置

	hub := proxy.NewHub()
	mode := viper.GetString("Mode")
	if mode == "release" || mode == "prod" {
		gin.SetMode(gin.ReleaseMode)
	}
	hub.Env = mode

	router := routes.InitRoute(hub)
	srv := &http.Server{
		Addr:    viper.GetString("addr"),
		Handler: router,
	}

	// 初始化 6.启动服务
	go func() {
		// service connections
		log.Infof("decs-mst running")
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			fmt.Println(err)
			log.Fatalf("listen: %s\n", err)
		}
		log.Infof("decs-mst exiting")
	}()

	fmt.Println("server is running...")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
