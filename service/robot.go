package service

import (
	"fmt"
	"strconv"
	"strings"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
)

func Run() {
	log.Info("机器人跑起来~")

	time.Sleep(time.Second * 5)

	// 随机操作
	go func() {
		for {
			buySellDTO := BuySellDTO{
				Type: 3,
			}
			handle(buySellDTO)
			time.Sleep(time.Minute * 30)
		}
	}()

	// // 模拟买入
	go func() {
		// for {
		buySellDTO := BuySellDTO{
			Type:    0,
			StockNo: "600225",
			Price:   2.45,
			Count:   100,
		}
		handle(buySellDTO)
		time.Sleep(time.Second * 10)
		// }
	}()

	// TCP
	for {
		err := Sock()
		if err != nil {
			log.Info("err", err)
		}
		time.Sleep(time.Second)
	}
}

// keybd_event 参数
const (
	KEYEVENTF_KEYDOWN = 0x0000 // 按下键
	KEYEVENTF_KEYUP   = 0x0002 // 释放键
)

var (
	user32         = syscall.NewLazyDLL("user32.dll")
	procKeybdEvent = user32.NewProc("keybd_event")
)

// BuySellDTO 模拟 Java 中的 BuySellDTO 类
type BuySellDTO struct {
	Type    int
	StockNo string
	Price   float64
	Count   int
}

func ReviceSell(input string) BuySellDTO {
	// 示例输入字符串: "0/002094/8.52"
	// input = "0/002094/9.21"

	// 解析字符串
	parts := strings.Split(input, "/")
	if len(parts) != 4 {
		// log.Info("输入格式错误")
		return BuySellDTO{}
	}

	// 获取当前时间
	currentTime := time.Now()

	// 打印当前时间
	log.Info("当前时间进来：", currentTime)

	// 创建 BuySellDTO 结构体
	var buySellDTO BuySellDTO

	// 解析并填充结构体
	var err error

	// 将字符串转换为 int64
	targetTimestamp, err := strconv.ParseInt(parts[3], 10, 64)
	if err != nil {
		fmt.Println("转换错误:", err)
		return BuySellDTO{}
	}

	// 获取当前 Unix 时间戳
	currentTimestamp := time.Now().Unix()

	if currentTimestamp > targetTimestamp {
		fmt.Println("当前时间戳大于目标时间戳")
		return BuySellDTO{}
	}

	buySellDTO.Type, err = strconv.Atoi(parts[0]) // 解析 Type
	if err != nil {
		log.Fatal("解析 Type 错误:", err)
	}
	buySellDTO.StockNo = parts[1]                            // 股票代码
	buySellDTO.Price, err = strconv.ParseFloat(parts[2], 64) // 解析 Price
	if err != nil {
		log.Fatal("解析 Price 错误:", err)
	}

	// 计算 Count 的最大值，使得 Count * Price <= 20000
	maxValue := 8000.0
	maxCount := int(maxValue / buySellDTO.Price)

	// 使 Count 为 100 的倍数，向下舍入
	if maxCount%100 != 0 {
		maxCount = maxCount / 100 * 100
	}

	// 如果计算出的 Count 为 0 且价格大于 20000，则报错
	if maxCount == 0 && buySellDTO.Price > maxValue {
		log.Fatal("价格过高，无法购买")
	}

	// 设置买入的 Count
	buySellDTO.Count = maxCount

	// 计算 Total Value
	totalValue := float64(buySellDTO.Count) * buySellDTO.Price

	// 输出结果
	fmt.Printf("买卖信息：%+v\n", buySellDTO)
	fmt.Printf("买入金额: %.2f\n", totalValue)

	handle(buySellDTO)

	return buySellDTO
}

// keybd_event 函数封装
func keybdEvent(bVk byte, bScan byte, dwFlags uint32, dwExtraInfo uint32) {
	procKeybdEvent.Call(uintptr(bVk), uintptr(bScan), uintptr(dwFlags), uintptr(dwExtraInfo))
}

// 模拟按键操作
func pressAndRelease(keyCode uint16) {
	// 模拟按键按下
	keybdEvent(byte(keyCode), 0, KEYEVENTF_KEYDOWN, 0)
	time.Sleep(5 * time.Millisecond) // 增加延迟，避免重复按键

	// 模拟按键释放
	keybdEvent(byte(keyCode), 0, KEYEVENTF_KEYUP, 0)
	time.Sleep(5 * time.Millisecond) // 增加延迟，避免重复按键
}

// 输入数字或小数
func enter(value string) {
	for _, c := range value {
		if c == '.' {
			pressAndRelease(0xBE) // 小数点
		} else if c >= '0' && c <= '9' {
			// 将字符转换为对应的数字键码
			pressAndRelease(uint16(0x30 + (c - '0')))
		}
	}
}

// 执行操作的逻辑
func handle(buySellDTO BuySellDTO) {
	// 打印当前时间
	log.Info("当前时间hand1：", time.Now())
	switch buySellDTO.Type {
	case 0: // 买入操作
		pressAndRelease(0x0D) // 回车
		pressAndRelease(0x72) // F3
		pressAndRelease(0x70) // F1
	case 1: // 卖出操作
		pressAndRelease(0x0D) // 回车
		pressAndRelease(0x72) // F3
		pressAndRelease(0x71) // F2
	case 2: // 取消操作
		pressAndRelease(0x72) // F3
		time.Sleep(500 * time.Millisecond)
		pressAndRelease(0x43) // C
		time.Sleep(500 * time.Millisecond)
		pressAndRelease(0x59) // Y
		return
	case 3: // 随机操作
		random()
		return
	case 4: // 强制卖出
		pressAndRelease(0x72) // F3
		// time.Sleep(10 * time.Millisecond)
		pressAndRelease(0x58) // X
		time.Sleep(10 * time.Millisecond)
		pressAndRelease(0x59) // Y
		return
	}

	// 输入股票代码
	enter(buySellDTO.StockNo)

	// 回车
	pressAndRelease(0x0D)

	pressAndRelease(0x08)
	pressAndRelease(0x08)
	pressAndRelease(0x08)
	pressAndRelease(0x08)
	pressAndRelease(0x08)

	// 输入价格
	enter(fmt.Sprintf("%.2f", buySellDTO.Price))

	// 下移
	pressAndRelease(0x0D)

	// 输入股票数量
	enter(fmt.Sprintf("%d", buySellDTO.Count))

	// 回车确认
	pressAndRelease(0x0D)
	pressAndRelease(0x0D)
	pressAndRelease(0x0D)

	log.Info("当前时间hand2：", time.Now())

	log.Println("完成命令！")
}

// 随机操作，按下多个功能键
func random() {
	pressAndRelease(0x0D) // 回车
	pressAndRelease(0x72) // F3
	pressAndRelease(0x74) // F5
}
