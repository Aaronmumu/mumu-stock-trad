package config

// 可以根据需要添加项目的配置信息，例如 API 地址等配置信息
// 在实际项目中，可以使用 viper 等工具管理配置信息

// 示例：定义 API 地址常量
const (
	ApiURL = "https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=%s"
)

// MySQL 数据库连接信息
const (
	DbUser     = "root"
	DbPassword = "root"
	DbName     = "stock"
	DbHost     = "localhost"
	DbPort     = "3306"
)
