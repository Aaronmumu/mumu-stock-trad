package proxy

import (
	"github.com/spf13/viper"
)

// Hub storing all driver
type Hub struct {
	Env string
}

// NewHub
func NewHub() *Hub {
	addr := viper.GetString("Redis")
	if addr == "" {
		addr = "localhost:6379"
	}

	return &Hub{}
}
