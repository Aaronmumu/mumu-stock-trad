/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : stock

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 12/09/2024 22:48:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `level` enum('top','sub','sub_sub') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name_code`(`name`, `code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 391 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_no` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '股票名称',
  `open_price` decimal(8, 3) NOT NULL COMMENT '开盘价格',
  `open_bfq_price` decimal(8, 3) NOT NULL DEFAULT 0.000 COMMENT '开盘价格 不复权',
  `close_price` decimal(8, 3) NOT NULL COMMENT '关盘价格',
  `close_bfq_price` decimal(8, 3) NOT NULL DEFAULT 0.000 COMMENT '不复权的收盘价',
  `diff_rate` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '涨跌幅度',
  `max_price` decimal(8, 3) NULL DEFAULT NULL,
  `max_bfq_price` decimal(8, 3) NOT NULL DEFAULT 0.000 COMMENT '不复权的最高价',
  `min_price` decimal(8, 3) NULL DEFAULT NULL,
  `min_bfq_price` decimal(8, 3) NOT NULL DEFAULT 0.000 COMMENT '不复权的最低价',
  `turnover_rate` decimal(8, 3) NULL DEFAULT NULL COMMENT '换手率 (百分比)',
  `static_pe` decimal(10, 3) NULL DEFAULT 0.000 COMMENT '静态市盈率',
  `pe` decimal(10, 3) NULL DEFAULT 0.000 COMMENT '动态市盈率',
  `ttm` decimal(10, 3) NULL DEFAULT 0.000 COMMENT '市盈率TTM',
  `volumn` decimal(18, 2) NULL DEFAULT NULL COMMENT '成交数',
  `trade_amount` decimal(18, 2) NULL DEFAULT NULL COMMENT '成交额（元）',
  `limit_up` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否涨停，1涨停，0不是',
  `limit_down` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否跌停，1跌停，0不是',
  `small_order` decimal(20, 3) NOT NULL DEFAULT 0.000 COMMENT '小单金额',
  `middle_order` decimal(20, 3) NOT NULL DEFAULT 0.000 COMMENT '小单金额',
  `big_order` decimal(20, 3) NOT NULL DEFAULT 0.000 COMMENT '大单金额',
  `large_order` decimal(20, 3) NOT NULL DEFAULT 0.000 COMMENT '特大单金额',
  `date` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_no_date`(`stock_no`, `date`) USING BTREE,
  INDEX `index_name_date`(`name`) USING BTREE,
  INDEX `index_date`(`date`, `stock_no`) USING BTREE,
  INDEX `index_date_limitUp`(`date`, `limit_up`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9607836 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '历史数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_data
-- ----------------------------
DROP TABLE IF EXISTS `stock_data`;
CREATE TABLE `stock_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键，唯一标识每条记录',
  `stock_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '股票名称',
  `trade_money` decimal(15, 2) NOT NULL COMMENT '交易金额',
  `diff_money` decimal(10, 2) NOT NULL COMMENT '价格差异金额',
  `open_price` decimal(10, 2) NOT NULL COMMENT '开盘价',
  `stock_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '股票代码',
  `date` date NOT NULL COMMENT '交易日期',
  `min_price` decimal(10, 2) NOT NULL COMMENT '最低价',
  `market` enum('sh','sz') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '市场（例如，`sh` 为上海，`sz` 为深圳）',
  `trade_num` int(11) NOT NULL COMMENT '交易数量',
  `close_price` decimal(10, 2) NOT NULL COMMENT '收盘价',
  `max_price` decimal(10, 2) NOT NULL COMMENT '最高价',
  `swing` decimal(5, 2) NOT NULL COMMENT '振幅',
  `diff_rate` decimal(5, 2) NOT NULL COMMENT '涨跌幅度',
  `turnover` decimal(5, 2) NOT NULL COMMENT '换手率',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_stock_date`(`stock_code`, `date`) USING BTREE COMMENT '唯一索引，确保同一天同一股票的唯一性'
) ENGINE = InnoDB AUTO_INCREMENT = 565078 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '股票数据表，存储股票交易数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_favorites
-- ----------------------------
DROP TABLE IF EXISTS `stock_favorites`;
CREATE TABLE `stock_favorites`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `stock_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stock_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 340 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '股票收藏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_list
-- ----------------------------
DROP TABLE IF EXISTS `stock_list`;
CREATE TABLE `stock_list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '股票代码',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '股票名称',
  `market` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '市场代码（sh, sz等）',
  `totalcapital_val` decimal(18, 6) NOT NULL COMMENT '总资本值',
  `currcapital_val` decimal(18, 6) NOT NULL COMMENT '当前资本值',
  `all_num` int(11) NULL DEFAULT NULL COMMENT '与股票相关的所有数量',
  `type_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '与股票相关的类型ID',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '与股票相关的分类名称',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间戳',
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '记录更新时间戳',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5619 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '存储股票信息的表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_qt
-- ----------------------------
DROP TABLE IF EXISTS `stock_qt`;
CREATE TABLE `stock_qt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL COMMENT '日期',
  `stock_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '股票代码',
  `stock_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '股票名称',
  `current_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前价格',
  `yesterday_close_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '昨收价',
  `today_open_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '今开价',
  `volume` int(11) NULL DEFAULT NULL COMMENT '成交量（手）',
  `outer_disk` int(11) NULL DEFAULT NULL COMMENT '外盘',
  `inner_disk` int(11) NULL DEFAULT NULL COMMENT '内盘',
  `buy_one_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '买一价',
  `buy_one_volume` int(11) NULL DEFAULT NULL COMMENT '买一量',
  `buy_two_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '买二价',
  `buy_two_volume` int(11) NULL DEFAULT NULL COMMENT '买二量',
  `buy_three_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '买三价',
  `buy_three_volume` int(11) NULL DEFAULT NULL COMMENT '买三量',
  `buy_four_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '买四价',
  `buy_four_volume` int(11) NULL DEFAULT NULL COMMENT '买四量',
  `buy_five_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '买五价',
  `buy_five_volume` int(11) NULL DEFAULT NULL COMMENT '买五量',
  `sell_one_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '卖一价',
  `sell_one_volume` int(11) NULL DEFAULT NULL COMMENT '卖一量',
  `sell_two_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '卖二价',
  `sell_two_volume` int(11) NULL DEFAULT NULL COMMENT '卖二量',
  `sell_three_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '卖三价',
  `sell_three_volume` int(11) NULL DEFAULT NULL COMMENT '卖三量',
  `sell_four_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '卖四价',
  `sell_four_volume` int(11) NULL DEFAULT NULL COMMENT '卖四量',
  `sell_five_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '卖五价',
  `sell_five_volume` int(11) NULL DEFAULT NULL COMMENT '卖五量',
  `latest_transaction` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最近逐笔成交',
  `transaction_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时间',
  `change_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '涨跌额',
  `change_percent` decimal(8, 2) NULL DEFAULT NULL COMMENT '涨跌幅',
  `highest_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最高价',
  `lowest_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价',
  `buy_sell_price_volume_buy` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格/成交量（手）（买入）',
  `buy_sell_price_volume_sell` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格/成交量（手）（卖出）',
  `turnover_amount` decimal(15, 2) NULL DEFAULT NULL COMMENT '成交额（万）',
  `turnover_volume` int(11) NULL DEFAULT NULL COMMENT '成交量',
  `pe_ratio` decimal(10, 2) NULL DEFAULT NULL COMMENT '市盈率',
  `capitalization` decimal(20, 2) NULL DEFAULT NULL COMMENT '股本（亿）',
  `total_market_value` decimal(20, 2) NULL DEFAULT NULL COMMENT '总市值（亿）',
  `circulating_market_value` decimal(20, 2) NULL DEFAULT NULL COMMENT '流通市值（亿）',
  `amplitude` decimal(8, 2) NULL DEFAULT NULL COMMENT '振幅',
  `turnover_rate` decimal(8, 2) NULL DEFAULT NULL COMMENT '换手率',
  `pb_ratio` decimal(10, 2) NULL DEFAULT NULL COMMENT '市净率',
  `commission_ratio` decimal(8, 2) NULL DEFAULT NULL COMMENT '委比',
  `volume_ratio` decimal(8, 2) NULL DEFAULT NULL COMMENT '量比',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_stock_code_date`(`stock_code`, `date`) USING BTREE,
  INDEX `idx_c`(`change_percent`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 342340 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '股票数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '股票代码',
  `stock_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '股票名称',
  `current` double NOT NULL COMMENT '当前价格',
  `percent` double NOT NULL COMMENT '涨跌幅百分比',
  `chg` double NOT NULL COMMENT '涨跌额',
  `timestamp` bigint(20) NOT NULL COMMENT '时间戳',
  `volume` double NOT NULL COMMENT '成交量',
  `amount` double NOT NULL COMMENT '成交额',
  `market_capital` double NOT NULL COMMENT '市值',
  `float_market_capital` double NOT NULL COMMENT '流通市值',
  `turnover_rate` double NOT NULL COMMENT '换手率',
  `amplitude` double NOT NULL COMMENT '振幅',
  `open` double NOT NULL COMMENT '开盘价',
  `last_close` double NOT NULL COMMENT '昨日收盘价',
  `high` double NOT NULL COMMENT '最高价',
  `low` double NOT NULL COMMENT '最低价',
  `avg_price` double NOT NULL COMMENT '平均价格',
  `current_year_percent` double NOT NULL COMMENT '今年涨跌幅百分比',
  `date` date NOT NULL COMMENT '日期',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `market` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '股票市场所属',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `symbol_date_unique_idx`(`symbol`, `date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储股票信息的表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
